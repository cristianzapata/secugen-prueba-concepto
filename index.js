let fingerOne = '';
let fingerTwo = '';
let fingerThree = '';

let register = () => {
	let id = $('#id').val();
	let name = $('#name').val();
	let color = $('#color').val();
	if (id.length > 0 && name.length > 0 && color.length > 0) {
		if (fingerOne.length > 0 && fingerTwo.length > 0 && fingerThree.length > 0) {
			let status = (users[id] !== null && users[id] !== undefined);
			let user = {
				'id': id,
				'name': name,
				'color': color,
				'finger': {
					'1': fingerOne,
					'2': fingerTwo,
					'3': fingerThree
				}
			};
			users[id] = user;
			localStorage.setItem('users', JSON.stringify(users));
			if (status) {
				alert('Usuario actualizado exitosamente');
			} else {
				alert('Usuario registrado exitosamente');
			}
			cleanFormRegister();
		} else {
			alert('Debe capturar la la huella dactilar de tres dedos');
		}
	} else {
		alert('Debe completar la información de contacto');
	}
};

let getFinger = (idx) => {
	ajaxConsume(`${urlBase}${urlCapture}`, 'POST', {}, true, (data) => {
		if (data.ErrorCode === 0) {
			if (data.ImageQuality >= 60) {
				if ($('#FPImage' + idx)) {
					if (data.TemplateBase64.length > 0) {
						$('#FPImage' + idx).attr('src', `data:image/bmp;base64,${data.BMPBase64}`);
						if (idx === '1') fingerOne = data.TemplateBase64;
						else if (idx === '2') fingerTwo = data.TemplateBase64;
						else if (idx === '3') fingerThree = data.TemplateBase64;
					} else {
						_error('Favor vuelva a capturar la huella');
					}
				}
			} else {
				_error('La calidad de la huella es baja, favor capturala de nuevo');
			}
		} else {
			_error('La captura dactilar arrojo lo siguiente. código de error = ' + (data.ErrorCode || 'Sin código'));
		}
	});
};

let cleanFormRegister = () => {
	$('#id').val('');
	$('#name').val('');
	$('#color').val('#000000');
	$('#FPImage1').attr('src', 'img/push.png');
	$('#FPImage2').attr('src', 'img/push.png');
	$('#FPImage3').attr('src', 'img/push.png');
};