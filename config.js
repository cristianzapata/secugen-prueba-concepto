let urlBase = 'http://localhost:8000/';
let urlCapture = 'SGIFPCapture';
let urlMatch = 'SGIMatchScore';

let users = {}; 

$(window).ready(() => {
	if (localStorage.getItem('users')) {
		console.log('Usuario cargados');
		users = JSON.parse(localStorage.getItem('users'));
	}
});

let _error = (value) => {
	if (value) {
		alert(value);
	} else {
		alert('Se presento un error inesperado.\nPruebe de nuevo en un instante.');
	}
};

let ajaxConsume = (url = String, method = String, data = Object, state = Boolean, callBack = any) => {
	$.ajax({
		url: url,
		async: state,
		type: method || 'GET',
		data: data || {},
		dataType: 'JSON',
		error: _error,
		success: (data) => {
			callBack(data);
		}
	});
};