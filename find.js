let find = () => {
	let idu = $('#find').val();
	if (idu.length > 0) {
		if (users[idu] !== null && users[idu] !== undefined) {
			let user = users[idu];
			ajaxConsume(`${urlBase}${urlCapture}`, 'POST', {}, true, (data) => {
				if (data.ErrorCode === 0) {
					if (data.ImageQuality >= 60) {
						$('#title').text('Buscar Usuaurio en proceso...');
						$('#finger').attr('src', `data:image/bmp;base64,${data.BMPBase64}`);
						setTimeout(() => {
							let count = Object.keys(user.finger).length;
							for (let i = 1; i <= count; i++) {
								let _data = {
									'template1': encodeURIComponent(data.TemplateBase64),
									'template2': encodeURIComponent(user.finger[`${i}`]),
									'templateFormat': 'ISO' 
								}
								ajaxConsume(`${urlBase}${urlMatch}`, 'POST', _data, false, (dt) => {
									if (dt.ErrorCode === 0 && dt.MatchingScore >= 170) {
										count = -1;
									}
								});
								if (count === -1) {
									break;
								}
							}
							if (count === -1) {
								alert('Usuario encontrado\nNombre: ' + user.name + '\nIdentificación: ' + user.id + '\nColor: ' + user.color);
							} else {
								alert('La huella dactilar no coincide con la del usuario');
							}
							$('#title').text('Buscar Usuaurio');
							$('#find').val('');
							$('#finger').attr('src', 'img/push.png');
						}, 3000);
					} else {
						_error('La calidad de la huella es baja, favor capturala de nuevo');
					}
				} else {
					_error('La captura dactilar arrojo lo siguiente. código de error = ' + (data.ErrorCode || 'Sin código'));
				}
			});
		} else {
			alert('La identificación ingresada no corresponde a ningún usuario.')
		}
	} else {
		alert('Debe ingresar el valor de la identificación')
	}
};